package com.test.idealista.config;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.bson.Document;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import cz.jirutka.spring.embedmongo.EmbeddedMongoFactoryBean;

@Configuration
public class MongoConfig {

	private static final String MONGO_DB_URL = "localhost";
	private static final String MONGO_DB_NAME = "embeded_db";

	@Bean
	public MongoTemplate mongoTemplate() throws IOException {
		EmbeddedMongoFactoryBean mongo = new EmbeddedMongoFactoryBean();
		mongo.setBindIp(MONGO_DB_URL);
		MongoClient mongoClient = mongo.getObject();
		MongoTemplate mongoTemplate = new MongoTemplate(mongoClient, MONGO_DB_NAME);

		mongoTemplate.createCollection("ads");
		mongoTemplate.createCollection("pictures");

		MongoCollection<Document> collection = mongoTemplate.getCollection("ads");
		MongoCollection<Document> collectionPictures = mongoTemplate.getCollection("pictures");

		collection.insertMany(getAnuncios());

		collectionPictures.insertMany(getPictures());

		return mongoTemplate;
	}

	private List<Document> getAnuncios() {

		Document document1 = new Document();
		document1.put("_id", "1");
		document1.put("description", "Este piso es una ganga, compra, compra, COMPRA!!!!!");
		document1.put("typology", "CHALET");
		document1.put("houseSize", "300");
		
		Document document2 = new Document();
		document2.put("_id", "2");
		document2.put("description", "Nuevo ático céntrico recién reformado. No deje pasar la oportunidad y adquiera este ático de lujo");
		document2.put("typology", "FLAT");
		document2.put("houseSize", "300");
		List<String> pictures2 = new ArrayList<>();
		pictures2.add("4");
		document2.put("pictures", pictures2);
		
		Document document3 = new Document();
		document3.put("_id", "3");
		document3.put("description", "");
		document3.put("typology", "CHALET");
		document3.put("houseSize", "210");
		document3.put("gardenSize", "25");
		List<String> pictures3 = new ArrayList<>();
		pictures3.add("2");
		document3.put("pictures", pictures3);
		
		Document document4 = new Document();
		document4.put("_id", "4");
		document4.put("description", "Ático céntrico muy luminoso y recién reformado, parece nuevo");
		document4.put("typology", "FLAT");
		document4.put("houseSize", "130");
		List<String> pictures4 = new ArrayList<>();
		pictures4.add("5");
		document4.put("pictures", pictures4);
		
		Document document5 = new Document();
		document5.put("_id", "5");
		document5.put("description", "Pisazo");
		document5.put("typology", "FLAT");
		List<String> pictures5 = new ArrayList<>();
		pictures5.add("3");
		pictures5.add("4");
		document5.put("pictures", pictures5);
		
		Document document6 = new Document();
		document6.put("_id", "6");
		document6.put("description", "");
		document6.put("typology", "GARAGE");
		List<String> pictures6 = new ArrayList<>();
		pictures6.add("6");
		document6.put("pictures", pictures6);
		
		Document document7 = new Document();
		document7.put("_id", "7");
		document7.put("description", "Garaje en el centro de Albacete");
		document7.put("typology", "GARAGE");
		
		Document document8 = new Document();
		document8.put("_id", "8");
		document8.put("description", "Maravilloso chalet situado en als afueras de un pequeño pueblo rural. El entorno es espectacular, las vistas magníficas. ¡Cómprelo ahora!");
		document8.put("typology", "CHALET");
		document8.put("houseSize", "150");
		document8.put("gardenSize", "20");
		List<String> pictures8 = new ArrayList<>();
		pictures8.add("1");
		pictures8.add("7");
		document8.put("pictures", pictures8);

		List<Document> documents = new ArrayList<>();
		documents.add(document1);
		documents.add(document2);
		documents.add(document3);
		documents.add(document4);
		documents.add(document5);
		documents.add(document6);
		documents.add(document7);
		documents.add(document8);
		return documents;
	}

	private List<Document> getPictures() {

		Document document1 = new Document();
		document1.put("_id", "1");
		document1.put("url", "http://www.idealista.com/pictures/1");
		document1.put("quality", "SD");
		
		Document document2 = new Document();
		document2.put("_id", "2");
		document2.put("url", "http://www.idealista.com/pictures/2");
		document2.put("quality", "HD");
		
		Document document3 = new Document();
		document3.put("_id", "3");
		document3.put("url", "http://www.idealista.com/pictures/3");
		document3.put("quality", "SD");
		
		Document document4 = new Document();
		document4.put("_id", "4");
		document4.put("url", "http://www.idealista.com/pictures/4");
		document4.put("quality", "HD");
		
		Document document5 = new Document();
		document5.put("_id", "5");
		document5.put("url", "http://www.idealista.com/pictures/5");
		document5.put("quality", "SD");
		
		Document document6 = new Document();
		document6.put("_id", "6");
		document6.put("url", "http://www.idealista.com/pictures/6");
		document6.put("quality", "SD");
		
		Document document7 = new Document();
		document7.put("_id", "7");
		document7.put("url", "http://www.idealista.com/pictures/7");
		document7.put("quality", "SD");
		
		List<Document> documents = new ArrayList<>();
		documents.add(document1);
		documents.add(document2);
		documents.add(document3);
		documents.add(document4);
		documents.add(document5);
		documents.add(document6);
		documents.add(document7);
		return documents;
	}

}
