package com.test.idealista.services;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.test.idealista.commons.Constants;
import com.test.idealista.models.Advert;
import com.test.idealista.repositories.AdvertRepository;
import com.test.idealista.repositories.PictureRepository;
import com.test.idealista.utils.StringOperations;

@Service
public class AdvertService {

	@Autowired
	private AdvertRepository repository;
	@Autowired
	private PictureRepository pictureRepository;

	public List<Advert> getIrrelevantAdverts() {

		return repository.findAdvertsByPuntuationBetween(-1, 40);

	}

	public List<Advert> getAdvertsForUsers() {

		Sort sort = new Sort(Sort.Direction.DESC, "points");
		return repository.findAll(sort);

	}

	public List<Advert> getAdverts() {

		return repository.findAll();

	}

	public List<Advert> setAdvertsPoints() {

		List<Advert> adverts = repository.findAll();

		for (Advert advert : adverts) {

			int points = getPoints(advert);
			advert.setPoints(points);
			if (points < 40) {
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
				advert.setIrrelevantDate(simpleDateFormat.format(new Date()));
			}
			repository.save(advert);
		}

		return repository.findAll();

	}

	private int getPoints(Advert a) {

		System.out.println("Anuncio: " + a.toString());
		int pointsImages = pointImages(a);
		System.out.println("pointsImages " + pointsImages);
		int pointsDescription = pointsDescription(a);
		System.out.println("pointsDescription " + pointsDescription);
		int pointsHotWords = pointsHotWords(a);
		System.out.println("pointsHotWords " + pointsHotWords);
		int pointsFullAd = pointsFullAd(a);
		System.out.println("pointsFullAd " + pointsFullAd);

		int total = pointsImages + pointsDescription + pointsHotWords + pointsFullAd;
		System.out.println("Puntuación total: " + total);
		return total;
	}

	private int pointImages(Advert anuncio) {

		int points = 0;

		List<String> picturesIds = anuncio.getPictures();

		if (picturesIds.size() == 0) {
			points -= 10;
		} else {
			for (String pictureId : picturesIds) {

				String quality = pictureRepository.findBy_id(pictureId).getQuality().toUpperCase();

				if (quality.equals(Constants.HD_TAG)) {
					points += 20;
				} else if (quality.equals(Constants.SD_TAG)) {
					points += 10;
				}
			}
		}
		return points;
	}

	private int pointsDescription(Advert anuncio) {

		int points = 0;

		int numPalabras = StringOperations.countWords(anuncio.getDescription());
		System.out.println("Numero de palabras: " + numPalabras);
		if (numPalabras != 0) {

			points += points + 5;

			switch (anuncio.getTypology()) {
			case Constants.FLAT_TYPOLOGY:
				if (numPalabras >= 50) {
					points += 30;
				} else if (numPalabras >= 20) {
					points += 10;
				}
				break;
			case Constants.CHALET_TYPOLOGY:
				if (numPalabras > 50) {
					points += 20;
				}
				break;
			default:
				break;
			}

		}

		return points;
	}

	private int pointsHotWords(Advert anuncio) {

		int points = 0;
		String description = StringUtils.stripAccents(anuncio.getDescription()).toUpperCase();

		for (String word : Constants.HOT_WORDS) {
			if (description.contains(word)) {
				points += 5;
			}
		}
		return points;
	}

	private int pointsFullAd(Advert anuncio) {

		int points = 0;

		boolean hasDescription = !anuncio.getDescription().isEmpty();
		boolean hasPics = anuncio.getPictures().size() != 0;
		boolean hasHouseDimen = !anuncio.getHouseSize().isEmpty();
		boolean hasGardenDimen = !anuncio.getGardenSize().isEmpty();

		switch (anuncio.getTypology()) {
		case Constants.FLAT_TYPOLOGY:
			if (hasDescription && hasPics && hasHouseDimen)
				points += 40;
			break;
		case Constants.CHALET_TYPOLOGY:
			if (hasDescription && hasPics && hasHouseDimen && hasGardenDimen)
				points += 40;
			break;
		case Constants.GARAGE_TYPOLOGY:
			if (hasPics)
				points += 40;
			break;
		default:
			break;
		}

		return points;

	}

}
