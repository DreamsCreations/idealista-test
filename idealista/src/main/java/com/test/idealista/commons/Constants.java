package com.test.idealista.commons;

public class Constants {

	public static final String FLAT_TYPOLOGY = "FLAT";
	public static final String CHALET_TYPOLOGY = "CHALET";
	public static final String GARAGE_TYPOLOGY = "GARAGE";
	
	public static final String [] HOT_WORDS = {"LUMINOSO", "NUEVO", "CENTRICO", "REFORMADO", "ATICO"};
	
	public static final String HD_TAG = "HD";
	public static final String SD_TAG = "SD";
}
