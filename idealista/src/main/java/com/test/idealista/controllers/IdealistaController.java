package com.test.idealista.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.test.idealista.models.Advert;
import com.test.idealista.services.AdvertService;

@RestController
public class IdealistaController {
	
	@Autowired
	private AdvertService advertService;

	@RequestMapping("/calculatePoints")
	public List<Advert> getAllAnuncios() {

        advertService.setAdvertsPoints();
        return advertService.getAdverts();
        
	}
	
	@RequestMapping("/users/showAllAdverts")
	public List<Advert> showAllAdverts() {
		
		return advertService.getAdvertsForUsers();
        
	}

	@RequestMapping("/quality/showIrrelevantAdverts")
	public List<Advert> showIrrelevantAdverts() {
		
		return advertService.getIrrelevantAdverts();
        
	}

}
