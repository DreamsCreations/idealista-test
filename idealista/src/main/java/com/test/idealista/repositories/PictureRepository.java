package com.test.idealista.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.test.idealista.models.Picture;

public interface PictureRepository extends MongoRepository<Picture, String> {
	Picture findBy_id(String _id);
}
