package com.test.idealista.repositories;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.test.idealista.models.Advert;

public interface AdvertRepository extends MongoRepository<Advert, String> {
	Advert findBy_id(String _id);
	
	@Query("{ 'points' : { $gt: ?0, $lt: ?1 } }")
	List<Advert> findAdvertsByPuntuationBetween(int ageGT, int ageLT);
}
