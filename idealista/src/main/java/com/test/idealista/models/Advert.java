package com.test.idealista.models;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "ads")
public class Advert {

	@Id
	public String _id;

	public String description;
	public String typology;
	public String houseSize;
	public String gardenSize;
	public List<String> pictures;
	public int points;
	
	private String irrelevantDate;

	public Advert() {

	}

	public Advert(String id, String description, String typology, String houseSize, String gardenSize,
			List<String> pictures) {

		this._id = id;
		this.description = description;
		this.typology = typology;
		this.houseSize = houseSize;
		this.gardenSize = gardenSize;
		this.pictures = pictures;
	}

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public String getDescription() {
		if (description == null) {
			return "";
		}
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTypology() {
		return typology;
	}

	public void setTypology(String typology) {
		this.typology = typology;
	}

	public String getHouseSize() {
		if (houseSize == null) {
			return "";
		}
		return houseSize;
	}

	public void setHouseSize(String houseSize) {
		this.houseSize = houseSize;
	}

	public String getGardenSize() {
		if (gardenSize == null) {
			return "";
		}
		return gardenSize;
	}

	public void setGardenSize(String gardenSize) {
		this.gardenSize = gardenSize;
	}

	public List<String> getPictures() {
		if (pictures == null) {
			return new ArrayList<>();
		}
		return pictures;
	}

	public void setPictures(List<String> pictures) {
		this.pictures = pictures;
	}

	public int getPoints() {
		return points;
	}

	public void setPoints(int puntuacion) {
		if (puntuacion < 0) {
			this.points = 0;
		} else if (puntuacion > 100) {
			this.points = 100;
		} else {
			this.points = puntuacion;
		}
	}

	public String getIrrelevantDate() {
		return irrelevantDate;
	}

	public void setIrrelevantDate(String irrelevantDate) {
		this.irrelevantDate = irrelevantDate;
	}

	@Override
	public String toString() {
		return "Anuncio [_id=" + _id + ", description=" + description + ", typology=" + typology + ", houseSize="
				+ houseSize + ", gardenSize=" + gardenSize + ", pictures=" + pictures + ", points=" + points
				+ "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((_id == null) ? 0 : _id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Advert other = (Advert) obj;
		if (_id == null) {
			if (other._id != null)
				return false;
		} else if (!_id.equals(other._id))
			return false;
		return true;
	}

}
